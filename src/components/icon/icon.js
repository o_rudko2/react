import React from "react";

const Icon = (icon, className, text) => {
    return (
        <div className={className}>
            <img src={icon}></img>
            <div>{text}</div>
        </div>
    )
}

export default Icon